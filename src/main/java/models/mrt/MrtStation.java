package main.java.models.mrt;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import main.java.fastestpath.models.Vertex;
import java.util.List;
import main.java.coords.LatLongPoint;
import main.java.fastestpath.commons.Pair;
import main.java.fastestpath.models.Edge;
import main.java.jackson.JsonMapKey;
import main.java.models.Location;

/**
 *
 * @author steinneuschwanstein
 */
public class MrtStation implements Vertex, Location {
    @JsonMapKey @JsonProperty("stationName") private String stationName;
    @JsonProperty("lines") private List<MrtStationInfo> lines;
    @JsonProperty("location") private LatLongPoint latlong;

    private List<Edge> edges = new ArrayList<>();
    private List<Pair<Vertex, Edge>> previous = new ArrayList<>();
    private double distanceFromSource = Double.MAX_VALUE;
    
    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }
    
    public List<MrtStationInfo> getLines() {
        return lines;
    }

    public void setLines(List<MrtStationInfo> lines) {
        this.lines = lines;
    }

    @Override
    public double getLatitude() {
        return latlong.getLatitude();
    }
    
    public void setLatitude(double latitude) {
        this.latlong.setLatitude(latitude);
    }
    
    @Override
    public double getLongitude() {
        return latlong.getLongitude();
    }
    
    public void setLongitude(double longitude) {
        this.latlong.setLongitude(longitude);
    }
    
    public LatLongPoint getLatLong() {
        return latlong;
    }

    public void setLatlng(LatLongPoint latlong) {
        this.latlong = latlong;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((stationName == null) ? 0 : stationName.hashCode());
        return result;
    }
  
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null)
            return false;
        if (getClass() != o.getClass())
            return false;
        MrtStation other = (MrtStation) o;
        if (stationName == null) {
            if (other.stationName != null)
                return false;
        } else if (!stationName.equals(other.stationName))
            return false;
        return true;
    }

    @Override
    public double getMinimumDistance() {
        return distanceFromSource;
    }

    @Override
    public void setMinimumDistance(double distance) {
        this.distanceFromSource = distance;
    }
    
    @Override
    public List<Pair<Vertex, Edge>> getPrevious() {
        return previous;
    }

    @Override 
    public void addPrev(Pair<Vertex, Edge> prev) {
        previous.add(prev);
    }
    
    @Override
    public int compareTo(Vertex other) {
        return Double.compare(getMinimumDistance(), other.getMinimumDistance());
    }

    @Override
    public List<Edge> getEdges() {
        return edges;
    }
    
    @Override
    public String toString() {
        return stationName + " (Mrt Station)";
    }
}