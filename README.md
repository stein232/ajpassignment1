# ST0316 Advanced Java Programming - Assignment 1

## Description
This is a project done in java for an assignment from AJP (AY 2016/2017 Sem 1).  
The project is a JavaFXML application created from NetBeans.

## Tools needed
* NetBeans 8.1

## Libraries used
* [GMapsFX](http://rterp.github.io/GMapsFX/)
* [Jackson](http://repo1.maven.org/maven2/com/fasterxml/jackson/core/)

## How to run this project
1. Git clone or download this repository
2. Go to netbeans and open the repository via 'Open Project'
3. Run the project in netbeans
