package main.java.fastestpath.models;

import java.util.List;
import main.java.fastestpath.commons.Pair;

public interface Vertex extends Comparable<Vertex> {    
    public double getMinimumDistance();
    public void setMinimumDistance(double distance);
    public List<Edge> getEdges();
    public List<Pair<Vertex, Edge>> getPrevious();
    public void addPrev(Pair<Vertex, Edge> prev);
}