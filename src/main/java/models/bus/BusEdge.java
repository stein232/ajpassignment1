package main.java.models.bus;

import main.java.AJPAssignment1;
import main.java.fastestpath.models.Edge;
import main.java.fastestpath.models.Vertex;

/**
 *
 * @author steinneuschwanstein
 */
public class BusEdge implements Edge {
    private String serviceNo;
    private String operator;
    private int direction;
    private Vertex source;
    private Vertex destination;
    private double distance;
    
    private static double AvgSpeed = 40; // 40km/hr
    
    public String getServiceNo() {
        return serviceNo;
    }

    public void setServiceNo(String serviceNo) {
        this.serviceNo = serviceNo;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }
    
    @Override
    public Vertex getDestination() {
        return destination;
    }

    public void setDestination(Vertex destination) {
        this.destination = destination;
    }
    
    @Override
    public Vertex getSource() {
        return source;
    }

    public void setSource(Vertex source) {
        this.source = source;
    }
    
    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }
    
    @Override
    public double getWeight() {
        return distance / AvgSpeed * 60 + 2;
    }

    @Override
    public double getLaneSwitchPenalty() {
        return 8;
    }

    @Override
    public boolean compareLane(Edge o) {
        if (this == o)
            return true;
        if (o == null)
            return false;
        if (getClass() != o.getClass())
            return false;
        
        BusEdge otherEdge = (BusEdge)o;
        
        return serviceNo.equals(otherEdge.serviceNo) && direction == otherEdge.direction;
    }
    
}
