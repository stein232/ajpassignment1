package main.java.viewmodels;

import com.lynden.gmapsfx.javascript.object.InfoWindow;
import com.lynden.gmapsfx.javascript.object.InfoWindowOptions;
import com.lynden.gmapsfx.javascript.object.LatLong;
import com.lynden.gmapsfx.javascript.object.MVCArray;
import com.lynden.gmapsfx.javascript.object.Marker;
import com.lynden.gmapsfx.javascript.object.MarkerOptions;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import main.java.fastestpath.commons.Pair;
import main.java.fastestpath.models.Edge;
import main.java.fastestpath.models.Vertex;
import main.java.models.Location;
import main.java.models.attraction.Attraction;
import main.java.models.bus.BusEdge;
import main.java.models.bus.BusStop;
import main.java.models.mrt.MrtEdge;
import main.java.models.mrt.MrtStation;
import main.java.models.walking.WalkingEdge;

/**
 *
 * @author steinneuschwanstein
 */
public class GooglePath {
    private LinkedList<Pair<Vertex, Edge>> path;
    private Vertex destination;
    private List<Marker> markers = new ArrayList<>();
    private List<InfoWindow> infoWindows = new ArrayList<>();
    private MVCArray coords = new MVCArray();
    private int change;
    
    public GooglePath(LinkedList<Pair<Vertex, Edge>> path, Vertex destination) {
        this.path = path;
        this.destination = destination;
        this.change = 0;
        createInstructionForPath();
    }

    public LinkedList<Pair<Vertex, Edge>> getPath() {
        return path;
    }

    public Vertex getDestination() {
        return destination;
    }

    public List<Marker> getMarkers() {
        return markers;
    }

    public List<InfoWindow> getInfoWindows() {
        return infoWindows;
    }

    public MVCArray getCoords() {
        return coords;
    }

    public int getChange() {
        return change;
    }

    public void setChange(int change) {
        this.change = change;
    }
    
    private void createInstructionForPath() {
        List<Integer> indexes = new ArrayList<>();
        indexes.add(0);
        for (int i = 0; i < path.size() - 1; i++) {
            Pair<Vertex, Edge> step = path.get(i);
            Pair<Vertex, Edge> nextStep = path.get(i+1);
            if (!step.getRight().compareLane(nextStep.getRight())) {
                indexes.add(i+1);
                change++;
            }
        }
        
        //Prepare the start and end
        Location start = (Location)path.getFirst().getLeft();
        Location end = (Location)destination;
        
        LatLong startLatlong = new LatLong(start.getLatitude(), start.getLongitude());
        MarkerOptions startMarkerOptions = new MarkerOptions();
        startMarkerOptions.position(startLatlong);
        Marker startMarker = new Marker(startMarkerOptions);

        LatLong endLatlong = new LatLong(end.getLatitude(), end.getLongitude());
        MarkerOptions endMarkerOptions = new MarkerOptions();
        endMarkerOptions.position(endLatlong);
        Marker endMarker = new Marker(endMarkerOptions);
        
        //Prepare the coords for polyline
        coords = new MVCArray();
        coords.push(startLatlong);
        for (int i = 0; i < path.size(); i++) {
            Pair<Vertex, Edge> step = path.get(i);
            Location location = (Location)step.getLeft();
            LatLong latlong = new LatLong(location.getLatitude(), location.getLongitude());
            coords.push(latlong);
        }
        coords.push(endLatlong);
        
        //create the markers and their corresponding infoWindow
        for (int i = 0; i < indexes.size(); i++) {
            Pair<Vertex, Edge> step = path.get(indexes.get(i));
            Location location = (Location)step.getLeft();
            LatLong latlong = new LatLong(location.getLatitude(), location.getLongitude());
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latlong);
            Marker marker = new Marker(markerOptions);
            markers.add(marker);
            
            int stopDifference;
            if (i+1 < indexes.size()) {
                //next stop change index - current stop change index
                stopDifference = indexes.get(i+1) - indexes.get(i);
            } else {
                //destinatiion index - current stop change index
                stopDifference = path.size() - indexes.get(i);
            }
            String instruction = "";

//            if (location.getClass() == MrtStation.class) {
//
//            } else if (location.getClass() == BusStop.class) {
//
//            } else if (location.getClass() == Attraction.class) {
//
//            }

            Edge edge = step.getRight();
            if (edge.getClass() == MrtEdge.class) {
                MrtEdge mrtEdge = (MrtEdge)edge;
                instruction = "Take the " + mrtEdge.getMrtLine()
                        + "\ntowards " + mrtEdge.getTowards() + " for " + stopDifference + "  stops";
            } else if (edge.getClass() == BusEdge.class) {
                BusEdge busEdge = (BusEdge)edge;
                instruction = "Take bus " + busEdge.getServiceNo()
                        + "\nat Busstop " + ((BusStop)busEdge.getSource()).getBusStopCode() + " for " + stopDifference + " stops";
            } else if (edge.getClass() == WalkingEdge.class) {
                instruction = "Walk from " + edge.getSource()
                        + "\nto " + edge.getDestination();
            }

            InfoWindowOptions infoWindowOptions = new InfoWindowOptions();
            infoWindowOptions.content(instruction);
            InfoWindow infoWindow = new InfoWindow(infoWindowOptions);
            infoWindows.add(infoWindow);
        }
        InfoWindowOptions infoWindowOptions = new InfoWindowOptions();
        infoWindowOptions.content("Reach destination");
        InfoWindow infoWindow = new InfoWindow(infoWindowOptions);
        infoWindows.add(infoWindow);
        markers.add(endMarker);
//        PolylineOptions polylineOptions = new PolylineOptions();
//        polylineOptions.path(coords);
//        polylineOptions.strokeColor("#33aaaa");
//        polylineOptions.strokeOpacity(1);
//        polylineOptions.strokeWeight(2);
//        polyline = new Polyline(polylineOptions);
//        map.addMapShape(polyline);
//        
//        markers.stream().forEach((marker) -> {
//            map.addMarker(marker);
//        });
    }
}
