package main.java.models.mrt;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author steinneuschwanstein
 */
public class MrtStationInfo {
    @JsonProperty("lineName") private String lineName;
    @JsonProperty("stationId") private String stationId;

    public String getLineName() {
        return lineName;
    }

    public void setLineName(String lineName) {
        this.lineName = lineName;
    }
    
    public String getStationId() {
        return stationId;
    }

    public void setStationId(String stationId) {
        this.stationId = stationId;
    }
}
