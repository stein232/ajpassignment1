package main.java.models.attraction;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.List;
import main.java.fastestpath.commons.Pair;
import main.java.fastestpath.models.Edge;
import main.java.fastestpath.models.Vertex;
import main.java.models.Location;

/**
 *
 * @author steinneuschwanstein
 */
public class Attraction implements Vertex, Location {

    @JsonProperty("name") private String name;
    @JsonProperty("url") private String url;
    @JsonProperty("iconName") private String iconName;
    @JsonProperty("latitude") private double latitude;
    @JsonProperty("longitude") private double longitude;

    private List<Edge> edges = new ArrayList<>();
    private List<Pair<Vertex, Edge>> previous = new ArrayList<>();
    private double distanceFromSource = Double.MAX_VALUE;
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getIconName() {
        return iconName;
    }

    public void setIconName(String iconName) {
        this.iconName = iconName;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
    
    @Override
    public double getMinimumDistance() {
        return distanceFromSource;
    }

    @Override
    public void setMinimumDistance(double distance) {
        this.distanceFromSource = distance;
    }

    @Override
    public List<Edge> getEdges() {
        return edges;
    }
    
    @Override
    public List<Pair<Vertex, Edge>> getPrevious() {
        return previous;
    } 

    @Override 
    public void addPrev(Pair<Vertex, Edge> prev) {
        previous.add(prev);
    }
    
    @Override
    public int compareTo(Vertex other) {
        return Double.compare(getMinimumDistance(), other.getMinimumDistance());
    }
    
    @Override
    public String toString()
    {
        return name;
    }
}
