package main.java.converters;

import java.util.List;
import javafx.util.StringConverter;
import main.java.models.Location;

/**
 *
 * @author steinneuschwanstein
 */
public class LocationConverter extends StringConverter<Location>{
    private List<Location> locations;
    
    public LocationConverter(List<Location> locations) {
        this.locations = locations;
    }
    
    @Override
    public String toString(Location location) {
        if (location == null) return null;
        return location.toString();
    }

    @Override
    public Location fromString(String string) {
        for (Location location : locations) {
            if (location.toString().equals(string)) {
                return location;
            }
        }
        return null;
    }

}
