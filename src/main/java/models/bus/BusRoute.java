package main.java.models.bus;

import com.fasterxml.jackson.annotation.*;

/**
 *
 * @author steinneuschwanstein
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BusRoute {
    @JsonProperty("ServiceNo") private String serviceNo;
    @JsonProperty("Operator") private String operator;
    @JsonProperty("Direction") private int direction;
    @JsonProperty("StopSequence") private int stopSequence;
    @JsonProperty("BusStopCode") private String busStopCode;
    @JsonProperty("Distance") private double distance;

    public String getServiceNo() {
        return serviceNo;
    }

    public void setServiceNo(String serviceNo) {
        this.serviceNo = serviceNo;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    public int getStopSequence() {
        return stopSequence;
    }

    public void setStopSequence(int stopSequence) {
        this.stopSequence = stopSequence;
    }

    public String getBusStopCode() {
        return busStopCode;
    }

    public void setBusStopCode(String busStopCode) {
        this.busStopCode = busStopCode;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }
}
