package main.java.models;

/**
 *
 * @author steinneuschwanstein
 */
public interface Location {
    public double getLatitude();
    public double getLongitude();
}
