package main.java.models.bus;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.List;
import main.java.fastestpath.commons.Pair;
import main.java.fastestpath.models.Edge;
import main.java.fastestpath.models.Vertex;
import main.java.jackson.JsonMapKey;
import main.java.models.Location;

/**
 *
 * @author steinneuschwanstein
 */
public class BusStop implements Vertex, Location {
    @JsonMapKey @JsonProperty("BusStopCode") private String busStopCode;
    @JsonProperty("RoadName") private String roadName;
    @JsonProperty("Description") private String description;
    @JsonProperty("Latitude") private double latitude;
    @JsonProperty("Longitude") private double longitude;
    
    private List<Edge> edges = new ArrayList<>();
    private List<Pair<Vertex, Edge>> previous = new ArrayList<>();
    private double distanceFromSource = Double.MAX_VALUE;

    public String getBusStopCode() {
        return busStopCode;
    }

    public void setBusStopCode(String busStopCode) {
        this.busStopCode = busStopCode;
    }

    public String getRoadName() {
        return roadName;
    }

    public void setRoadName(String roadName) {
        this.roadName = roadName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((busStopCode == null) ? 0 : busStopCode.hashCode());
        return result;
    }
  
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
          return true;
        if (obj == null)
          return false;
        if (getClass() != obj.getClass())
          return false;
        BusStop other = (BusStop) obj;
        if (busStopCode == null) {
          if (other.busStopCode != null)
            return false;
        } else if (!busStopCode.equals(other.busStopCode))
          return false;
        return true;
    }

    @Override
    public double getMinimumDistance() {
        return distanceFromSource;
    }

    @Override
    public void setMinimumDistance(double distance) {
        this.distanceFromSource = distance;
    }
    
    @Override
    public List<Edge> getEdges() {
        return edges;
    }

    @Override
    public List<Pair<Vertex, Edge>> getPrevious() {
        return previous;
    }
    
    @Override 
    public void addPrev(Pair<Vertex, Edge> prev) {
        previous.add(prev);
    }
    
    @Override
    public int compareTo(Vertex other) {
        return Double.compare(getMinimumDistance(), other.getMinimumDistance());
    }
    
    @Override
    public String toString() {
        return description + ", " + busStopCode + " (Bus stop)";
    }
}
