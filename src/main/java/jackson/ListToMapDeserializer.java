package main.java.jackson;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.BeanDeserializerBuilder;
import com.fasterxml.jackson.databind.deser.BeanDeserializerFactory;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author steinneuschwanstein
 * 
 */
public class ListToMapDeserializer<T> extends JsonDeserializer<Map<String, T>> {

    private Class<T> typeParameterClass;
    
    public ListToMapDeserializer(Class<T> typeParameterClass) {
        this.typeParameterClass = typeParameterClass;
    }
    
    @Override
    public Map<String, T> deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        JsonNode nodeArray = jp.getCodec().readTree(jp);
        Map<String, T> map = null;
        if (nodeArray.isArray()) {
            map = new HashMap<>();
            System.out.println(typeParameterClass);
            Field[] fields = typeParameterClass.getDeclaredFields();
            Field keyField = null;
            for (Field field : fields) {
                if (field.getAnnotation(JsonMapKey.class) != null) {
                    keyField = field;
                    break;
                }
            }
            for (JsonNode node : nodeArray) {
                
            }
        } else {
            throw new IOException("The json is not in the proper format of an array.");
        }
        return map;
    }
}
