package main.java.models.mrt;

import main.java.fastestpath.models.Edge;
import main.java.fastestpath.models.Vertex;

/**
 *
 * @author steinneuschwanstein
 */
public class MrtEdge implements Edge {
    private Vertex source;
    private Vertex destination;
    private String mrtLine;
    private double time;
    
    private int direction;
    private String towards;

    public void setSource(Vertex source) {
        this.source = source;
    }

    @Override
    public Vertex getSource() {
        return source;
    }
    
    public void setDestination(Vertex destination) {
        this.destination = destination;
    }

    @Override
    public Vertex getDestination() {
        return destination;
    }

    public String getMrtLine() {
        return mrtLine;
    }

    public void setMrtLine(String mrtLine) {
        this.mrtLine = mrtLine;
    }
    
    public void setWeight(double time) {
        this.time = time;
    }

    @Override
    public double getWeight() {
        return time;
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    @Override
    public double getLaneSwitchPenalty() {
        return 4;
    }

    @Override
    public boolean compareLane(Edge o) {
        if (this == o)
            return true;
        if (o == null)
            return false;
        if (getClass() != o.getClass())
            return false;
        
        MrtEdge otherEdge = (MrtEdge)o;
        
        return mrtLine.equals(otherEdge.mrtLine) && direction == otherEdge.direction;
    }

    public String getTowards() {
        return towards;
    }

    public void setTowards(String towards) {
        this.towards = towards;
    }
}
