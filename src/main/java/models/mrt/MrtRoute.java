package main.java.models.mrt;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author steinneuschwanstein
 */
public class MrtRoute {
    @JsonProperty("source") private String sourceStation;
    @JsonProperty("destination") private String destinationStation;
    @JsonProperty("mrtLine") private String mrtLine;
    @JsonProperty("time") private double time;
    @JsonProperty("direction") private int direction;
    @JsonProperty("towards") private String towards;

    public MrtRoute() { }
    
    public MrtRoute(String sourceStation, String destinationStation, double time, int direction) {
        this.sourceStation = sourceStation;
        this.destinationStation = destinationStation;
        this.time = time;
        this.direction = direction;
    }
    
    public String getSourceStation() {
        return sourceStation;
    }

    public void setSourceStation(String sourceStation) {
        this.sourceStation = sourceStation;
    }

    public String getDestinationStation() {
        return destinationStation;
    }

    public void setDestinationStation(String destinationStation) {
        this.destinationStation = destinationStation;
    }

    public String getMrtLine() {
        return mrtLine;
    }

    public void setMrtLine(String mrtLine) {
        this.mrtLine = mrtLine;
    }

    public double getTime() {
        return time;
    }

    public void setTime(double time) {
        this.time = time;
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    public String getTowards() {
        return towards;
    }

    public void setTowards(String towards) {
        this.towards = towards;
    }
}
