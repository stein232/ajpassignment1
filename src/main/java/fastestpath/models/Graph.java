package main.java.fastestpath.models;

import java.util.List;

public class Graph {
	private final List<Vertex> vertexes;

	public Graph(List<Vertex> vertexes) {
		this.vertexes = vertexes;
	}

	public List<Vertex> getVertexes() {
		return vertexes;
	}
}