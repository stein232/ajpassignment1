package main.java.fastestpath.engine;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.PriorityQueue;

import main.java.fastestpath.models.*;
import main.java.fastestpath.commons.Pair;

public class FastestPathAlgorithm {
    private final List<Vertex> vertexes;
    private PriorityQueue<Vertex> vertexQueue;
    private Map<Vertex, Pair<Vertex, Edge>> predecessors;
    private Set<Vertex> visitedNodes;
    private Vertex source;
//    private Set<LinkedList<Pair<Vertex,Edge>>> allPaths;
    private Set<LinkedList<Vertex>> allPaths;

    public FastestPathAlgorithm(Graph graph) {
        this.vertexes = new ArrayList<>(graph.getVertexes());
    }

    public void execute(Vertex source) {
        this.source = source;
        vertexQueue = new PriorityQueue<>();
        predecessors = new HashMap<>();
        visitedNodes = new HashSet<>();
        vertexes.stream().forEach((vertex) -> {
            vertex.setMinimumDistance(Double.MAX_VALUE);
        });
        source.setMinimumDistance(0.0);
        vertexQueue.add(source);
        while (!vertexQueue.isEmpty()) {
            Vertex node = vertexQueue.poll();
            findMinimalDistances(node);
        }
    }

    private void findMinimalDistances(Vertex node) {
        for (Edge edge : node.getEdges()) {
            Vertex target = edge.getDestination();            
            double distanceFromSourceToTarget = node.getMinimumDistance() + edge.getWeight();
            Pair<Vertex, Edge> previous = predecessors.get(node);
            if (previous != null && !previous.getRight().compareLane(edge)) {
                distanceFromSourceToTarget += edge.getLaneSwitchPenalty();
            }
            
            if (target.getMinimumDistance() > distanceFromSourceToTarget) {
                vertexQueue.remove(target);
                predecessors.put(target, new Pair<>(node, edge));
                target.setMinimumDistance(distanceFromSourceToTarget);
                vertexQueue.add(target);
            }
        }
    }
    
    public LinkedList<Pair<Vertex, Edge>> getShortestPath(Vertex target) {
        Pair<Vertex, Edge> step = predecessors.get(target);
        // check if a path exists
        if (step == null) {
            return null;
        }
        
        LinkedList<Pair<Vertex, Edge>> path = new LinkedList<>();
        path.add(step);
        step = predecessors.get(step.getLeft());
        while (step != null) {
            path.add(step);
            step = predecessors.get(step.getLeft());
        }
        
        Collections.reverse(path);
        return path;
    }
    
    public void getAllPath(Vertex target) {
        Pair<Vertex, Edge> step = predecessors.get(target);
        // check if a path exists
        if (step == null) {
            //return null;
        }
        allPaths = new HashSet<>();
        getAllPathUtil(target, new LinkedList<Vertex>());
        //return allPaths;
    }
    
    private void getAllPathUtil(Vertex vertex, LinkedList<Vertex> path) {
        visitedNodes.add(vertex);
        path.add(vertex);
        if (!vertex.equals(source)) {
            for (Edge edge : vertex.getEdges()) {
                Vertex target = edge.getDestination();
                if (!visitedNodes.contains(target)) {
        //            Pair<Vertex, Edge> step = new Pair<>(target, edge);
                    //path.add(target);
                    getAllPathUtil(target, path);
                }
            }
        } else {
//                LinkedList<Pair<Vertex, Edge>> donePath = new LinkedList<>();
            LinkedList<Vertex> donePath = new LinkedList<>();
            donePath.addAll(path);
            Collections.reverse(donePath);
            System.out.println("Got Path");
            allPaths.add(donePath);
        }
        path.remove(vertex);
        visitedNodes.remove(vertex);
    }
}