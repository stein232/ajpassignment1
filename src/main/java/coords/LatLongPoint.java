package main.java.coords;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author steinneuschwanstein
 */
public class LatLongPoint {
    @JsonProperty("lat") private double latitude;
    @JsonProperty("lng") private double longitude;

    public static final double EarthRadiusMeters = 6378137.0; // meters
    
    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
    
    public static double distance(double lat1, double long1, double lat2, double long2) {
        double dLat = (lat2 - lat1) * Math.PI / 180;
        double dLon = (long2 - long1) * Math.PI / 180;

        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180)
                * Math.sin(dLon / 2) * Math.sin(dLon / 2);

        double c = 2.0 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double d = EarthRadiusMeters * c;
        return d;
    }
    
    public static double distance(LatLongPoint latlong1, LatLongPoint latlong2) {
        return distance(latlong1.getLatitude(), latlong1.getLongitude(), latlong2.getLatitude(), latlong2.getLongitude());
    }
    
    public static double distance(LatLongPoint latlong1, double lat2, double long2) {
        return distance(latlong1.getLatitude(), latlong1.getLongitude(), lat2, long2);
    }
}
