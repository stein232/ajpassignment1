package main.java.models.walking;

import main.java.fastestpath.models.Edge;
import main.java.fastestpath.models.Vertex;

/**
 *
 * @author steinneuschwanstein
 */
public class WalkingEdge implements Edge {
    private Vertex source;
    private Vertex destination;
    private double distance; //meters
    
    // actual: 4650m/hr, compensate for going through buildings: 2500m/hr
    private static double AvgSpeed = 2500;
    
    @Override
    public Vertex getDestination() {
        return destination;
    }

    public void setDestination(Vertex destination) {
        this.destination = destination;
    }
    
    @Override
    public Vertex getSource() {
        return source;
    }

    public void setSource(Vertex source) {
        this.source = source;
    }
    
    @Override
    public double getWeight() {
        return distance / AvgSpeed * 60;
    }
    
    public void setDistance(double distance) {
        this.distance = distance;
    }

    @Override
    public double getLaneSwitchPenalty() {
        return 0;
    }

    @Override
    public boolean compareLane(Edge o) {
        return false;
    }
}
