package main.java.fastestpath.models;

public interface Edge<T> {
	public Vertex getDestination();

	public Vertex getSource();
	
	public double getWeight();

	public double getLaneSwitchPenalty();

	public boolean compareLane(Edge o);
}