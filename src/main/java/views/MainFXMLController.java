/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.java.views;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.MapComponentInitializedListener;
import com.lynden.gmapsfx.javascript.object.GoogleMap;
import com.lynden.gmapsfx.javascript.object.InfoWindow;
import com.lynden.gmapsfx.javascript.object.LatLong;
import com.lynden.gmapsfx.javascript.object.MapOptions;
import com.lynden.gmapsfx.javascript.object.MapTypeIdEnum;
import com.lynden.gmapsfx.javascript.object.Marker;
import com.lynden.gmapsfx.shapes.Polyline;
import com.lynden.gmapsfx.shapes.PolylineOptions;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import main.java.viewmodels.GooglePath;
import main.java.controls.AutoCompleteComboBoxListener;
import main.java.converters.LocationConverter;
import main.java.coords.LatLongPoint;
import main.java.fastestpath.commons.Pair;
import main.java.fastestpath.engine.FastestPathAlgorithm;

import main.java.fastestpath.models.Edge;
import main.java.fastestpath.models.Graph;
import main.java.fastestpath.models.Vertex;
import main.java.jackson.JsonMapKey;
import main.java.models.Location;
import main.java.models.attraction.Attraction;
import main.java.models.walking.WalkingEdge;
import main.java.models.bus.BusEdge;
import main.java.models.bus.BusRoute;
import main.java.models.bus.BusStop;
import main.java.models.mrt.MrtEdge;
import main.java.models.mrt.MrtRoute;
import main.java.models.mrt.MrtStation;

/**
 * FXML Controller class
 *
 * @author steinneuschwanstein
 */
public class MainFXMLController implements Initializable, MapComponentInitializedListener {

    @FXML private GoogleMapView mapView;
    @FXML private ComboBox<Location> sourceCB;
    @FXML private ComboBox<Location> destinationCB;
    @FXML private Button getDirectionsBtn;
    
    private GoogleMap map;
    private List<MrtStation> mrtStations;
    private Map<String, MrtStation> mrtStationsMap;
    private List<BusStop> busStops;
    private Map<String, BusStop> busStopsMap;
    private List<Attraction> attractions;
    private List<Vertex> allVertex;
    private List<Location> allLocations;
    private Graph graph;
    private List<Marker> markers;
    private List<InfoWindow> infoWindows;
    private Polyline polyline;
    private GooglePath shortestGooglePath;
    private List<GooglePath> allGooglePaths;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        mapView.addMapInializedListener(this);
        
        try {
            initFastestPath();
            System.out.println("init fastestpath done");
        } catch(IOException e) {
            e.printStackTrace();
        }
        
        ObservableList<Location> observableList = FXCollections.observableArrayList();
        observableList.addAll(allLocations);
        sourceCB.setItems(observableList);
        destinationCB.setItems(observableList);
        new AutoCompleteComboBoxListener(sourceCB);
        new AutoCompleteComboBoxListener(destinationCB);
        sourceCB.setConverter(new LocationConverter(allLocations));
        destinationCB.setConverter(new LocationConverter(allLocations));
    }
    
    @Override
    public void mapInitialized() {
         //Set the initial properties of the map.
        MapOptions mapOptions = new MapOptions();
        
        mapOptions.center(new LatLong(1.3521, 103.8198))
            .mapType(MapTypeIdEnum.ROADMAP)
            .overviewMapControl(false)
            .panControl(false)
            .rotateControl(false)
            .scaleControl(false)
            .streetViewControl(false)
            .zoomControl(false)
            .zoom(12);
                   
        map = mapView.createMap(mapOptions);
        
        markers = new ArrayList<>();
        polyline = new Polyline();
//        show all mrt stations
//        mrtStations.stream().forEach((station) -> {
//            LatLong latlong = new LatLong(station.getLatLong().getLatitude(), station.getLatLong().getLongitude());
//            MarkerOptions markerOptions = new MarkerOptions();
//            markerOptions.position(latlong);
//            Marker marker = new Marker(markerOptions);
//            map.addMarker(marker);
//        });
    }
    
    private void initFastestPath() throws IOException {
        ObjectMapper mapper = new ObjectMapper();

        long t1 = System.currentTimeMillis();
        
        mrtStations = mapper.readValue(getClass().getClassLoader().getResourceAsStream("main/resources/MRT stations.json"), new TypeReference<List<MrtStation>>() { });
        List<MrtRoute> mrtRoutes = mapper.readValue(getClass().getClassLoader().getResourceAsStream("main/resources/MRT routes.json"), new TypeReference<List<MrtRoute>>() { });
        
        mrtStationsMap = convertListToMap(mrtStations, MrtStation.class);
        List<MrtEdge> mrtEdges = generateMrtEdges(mrtRoutes, mrtStationsMap);
        assignEdges(convertToListOfVertex(mrtStations), convertToListOfEdge(mrtEdges));
        
        System.out.println("Time to process mrt json: " + (System.currentTimeMillis() - t1) + "ms");
//        Graph mrtGraph = new Graph(convertToListOfVertex(mrtStations));
//        FastestPathAlgorithm mrtFpa = new FastestPathAlgorithm(mrtGraph);
//        mrtFpa.execute(mrtStationsMap.get("Beauty World"));
//        LinkedList<Pair<Vertex, Edge>> mrtPath =  mrtFpa.getPath(mrtStationsMap.get("Stadium"));
//        mrtPath.stream().forEach((step) -> {
//           System.out.println(((MrtStation)step.getLeft()).getStationName()); 
//        });
//        System.out.println(mrtStationsMap.get("Stadium").getMinimumDistance());
        
        t1 = System.currentTimeMillis();

        busStops = mapper.readValue(getClass().getClassLoader().getResourceAsStream("main/resources/Bus stops.json"), new TypeReference<List<BusStop>>() { });
        List<BusRoute> busRoutes = mapper.readValue(getClass().getClassLoader().getResourceAsStream("main/resources/Bus routes.json"), new TypeReference<List<BusRoute>>() { });
        
        busStopsMap = convertListToMap(busStops, BusStop.class);
        List<BusEdge> busEdges = generateBusEdges(busRoutes, busStopsMap);
        assignEdges(convertToListOfVertex(busStops), convertToListOfEdge(busEdges));

        System.out.println("Time to process bus json: " + (System.currentTimeMillis() - t1) + "ms");
//        Graph busGraph = new Graph(convertToListOfVertex(busStops));
//        FastestPathAlgorithm busFpa = new FastestPathAlgorithm(busGraph);
//        busFpa.execute(busStopsMap.get("82029"));
//        LinkedList<Pair<Vertex, Edge>> busPath =  busFpa.getPath(busStopsMap.get("17171"));
//        busPath.stream().forEach((step) -> {
//            System.out.println(((BusStop)step.getLeft()).getBusStopCode() + ", " + ((BusEdge)step.getRight()).getServiceNo() + ", " + step.getLeft().getMinimumDistance());
//        });
//        System.out.println(busStopsMap.get("17171").getMinimumDistance());

        t1 = System.currentTimeMillis();

        //create attraction vertexes
        List<Attraction> hotels = mapper.readValue(getClass().getClassLoader().getResourceAsStream("main/resources/Hotels.json"), new TypeReference<List<Attraction>>() { });
        List<Attraction> historicSites = mapper.readValue(getClass().getClassLoader().getResourceAsStream("main/resources/HistoricSites.json"), new TypeReference<List<Attraction>>() { });
        List<Attraction> heritage = mapper.readValue(getClass().getClassLoader().getResourceAsStream("main/resources/Heritage.json"), new TypeReference<List<Attraction>>() { });
        List<Attraction> musuems = mapper.readValue(getClass().getClassLoader().getResourceAsStream("main/resources/Musuems.json"), new TypeReference<List<Attraction>>() { });
        List<Attraction> hawker = mapper.readValue(getClass().getClassLoader().getResourceAsStream("main/resources/Hawker.json"), new TypeReference<List<Attraction>>() { });

        attractions = hotels;
        attractions.addAll(historicSites);
        attractions.addAll(heritage);
        attractions.addAll(musuems);
        attractions.addAll(hawker);
        
        System.out.println("Time to process attractions json: " + (System.currentTimeMillis() - t1) + "ms");
        
        t1 = System.currentTimeMillis();
        
        //combine all vertex
        allVertex = convertToListOfVertex(attractions);
        allVertex.addAll(convertToListOfVertex(mrtStations));
        allVertex.addAll(convertToListOfVertex(busStops));
//        allVertex = convertToListOfVertex(mrtStations);
                
        System.out.println("Time to combine all vertexes: " + (System.currentTimeMillis() - t1) + "ms");
        
        t1 = System.currentTimeMillis();
        
        //create the walking edges
        allLocations = convertToListOfLocation(allVertex);
        allLocations.stream().forEach(location -> {
            Vertex vertex = (Vertex)location;
            allLocations.stream().filter(otherLocation -> {
                return !location.equals(otherLocation);// && location.getClass() != otherLocation.getClass(); 
            }).forEach(otherLocation -> {
                double distance = LatLongPoint.distance(location.getLatitude(), location.getLongitude(), otherLocation.getLatitude(), otherLocation.getLongitude());
                if (distance <= 300) {
                    Vertex otherVertex = (Vertex)otherLocation;
                    
                    WalkingEdge walkingEdge = new WalkingEdge();
                    walkingEdge.setSource(vertex);
                    walkingEdge.setDestination(otherVertex);
                    walkingEdge.setDistance(distance);
                    vertex.getEdges().add(walkingEdge);
                }
            });
        });
        System.out.println("Time to create walking edges: " + (System.currentTimeMillis() - t1) + "ms");

        //create the graph
        graph = new Graph(allVertex);
    }
    
    @FXML protected void getDirections(ActionEvent event) {
        FastestPathAlgorithm fpa = new FastestPathAlgorithm(graph);
        Vertex source = (Vertex)sourceCB.getSelectionModel().getSelectedItem();
        Vertex destination = (Vertex)destinationCB.getSelectionModel().getSelectedItem();
        
        if (source.equals(destination)) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Dafuq");
            alert.setHeaderText("Why u put same place for both 'From' and 'To'");
            alert.showAndWait();
            return;
        }
        
        long t1 = System.currentTimeMillis();
        
        fpa.execute(source);
        LinkedList<Pair<Vertex, Edge>> shortestPath = fpa.getShortestPath(destination);
        
        System.out.println("Time to get direction:" + (System.currentTimeMillis() - t1) + "ms");
        
        if (shortestPath == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Sorry");
            alert.setHeaderText("Sorry, we could not find any route");
            alert.showAndWait();
            return;
        }
        
        //print the path out
        shortestPath.stream().forEach((step) -> {
            Vertex vertex = step.getLeft();
            if (vertex.getClass() == BusStop.class) {
                BusStop busStop = (BusStop)vertex;
                System.out.print(busStop.getBusStopCode() + ", " + busStop.getMinimumDistance());
            } else if (vertex.getClass() == MrtStation.class) {
                MrtStation mrtStation = (MrtStation)vertex;
                System.out.print(mrtStation.getStationName() + ", " + mrtStation.getMinimumDistance());
            } else if (vertex.getClass() == Attraction.class) {
                Attraction attraction = (Attraction)vertex;
                System.out.print(attraction.getName() + ", " + attraction.getMinimumDistance());
            }
            
            Edge edge = step.getRight();
            if (edge.getClass() == BusEdge.class) {
                System.out.println(", " + ((BusEdge)edge).getServiceNo());
            } else if (edge.getClass() == WalkingEdge.class) {
                System.out.println(", walk");
            } else {
                System.out.println();
            }
        });
        System.out.println(destination.getMinimumDistance());
        
        //clear markers and polylines
        for (int i = 0; i < markers.size(); i++) {
            map.removeMarker(markers.get(i));
        }
        markers.clear();
        map.removeMapShape(polyline);
        
        //show the path on the map using markers and polyline
        shortestGooglePath = new GooglePath(shortestPath, destination);
        markers = shortestGooglePath.getMarkers();
        infoWindows = shortestGooglePath.getInfoWindows();
        
        PolylineOptions polylineOptions = new PolylineOptions();
        polylineOptions.path(shortestGooglePath.getCoords());
        polylineOptions.strokeColor("#33aaaa");
        polylineOptions.strokeOpacity(1);
        polylineOptions.strokeWeight(2);
        polyline = new Polyline(polylineOptions);
        map.addMapShape(polyline);
        
        markers.stream().forEach((marker) -> {
            map.addMarker(marker);
        });
        
        //cheap hack to fix markers that remain in map despite being removed
        int zoom = map.getZoom();
        map.setZoom(zoom+1);
        map.setZoom(zoom);
        
        //open the info windows
        for (int i = 0; i < markers.size(); i++) {
            Marker marker = markers.get(i);
            InfoWindow infoWindow = infoWindows.get(i);
            infoWindow.open(map, marker);
        }
        
        //fpa.getAllPath(destination);
//        allGooglePaths = new ArrayList<>();
//        allPaths.stream().forEach((path) -> {
//            GooglePath googlePath = new GooglePath(path, destination);
//            allGooglePaths.add(googlePath);
//        });
//        System.out.println("Got all paths to destination");
    }
    
    public static <T> List<Vertex> convertToListOfVertex(List<T> list) {
        List<Vertex> listObject = new ArrayList<>();
        list.stream().forEach((t) -> {
            listObject.add((Vertex)t);
        });
        
        return listObject;
    }
    
    public static <T> List<Edge> convertToListOfEdge(List<T> list) {
        List<Edge> listEdge = new ArrayList<>();
        list.stream().forEach((t) -> {
            listEdge.add((Edge)t);
        });
        
        return listEdge;
    }
    
    public static <T> List<Location> convertToListOfLocation(List<T> list) {
        List<Location> listLocation = new ArrayList<>();
        list.stream().forEach((u) -> {
            listLocation.add((Location)u);
        });
        
        return listLocation;
    }
    
    public static <T> Map<String, T> convertListToMap(List<T> list, Class<T> clazz) {
        Map<String, T> map = new HashMap<>();
        Field[] fields = clazz.getDeclaredFields();
        Field keyField = null;
        for (Field field : fields) {
            if (field.getAnnotation(JsonMapKey.class) != null) {
                keyField = field;
                break;
            }
        }
        
        if (keyField == null) {
            throw new NullPointerException(clazz.getName() + " does not have a field with JsonMapKey annotation");
        }
        
        keyField.setAccessible(true);
        
        for (T t : list) {
            String jsonMapKey = null;
            try {
                jsonMapKey = (String)keyField.get(t);
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                System.out.println("list to map conversion error");
            }
            
            map.put(jsonMapKey, t);
        }
        
        return map;
    }
    
    public static List<BusEdge> generateBusEdges(List<BusRoute> busRoutes, Map<String, BusStop> busStops) {
        List<BusEdge> busEdges = new ArrayList<>();
        
        for (int i = 0; i < busRoutes.size() - 1; i++) {
            
            BusRoute currentBusRoute = busRoutes.get(i);
            BusRoute nextBusRoute = busRoutes.get(i + 1);
            if (!currentBusRoute.getServiceNo().equals(nextBusRoute.getServiceNo()) || currentBusRoute.getDirection() != nextBusRoute.getDirection()) {
                continue;
            }
            
            BusEdge busEdge = new BusEdge();
            busEdge.setServiceNo(currentBusRoute.getServiceNo());
            busEdge.setOperator(currentBusRoute.getOperator());
            busEdge.setDirection(currentBusRoute.getDirection());
            busEdge.setSource((Vertex)busStops.get(currentBusRoute.getBusStopCode()));
            busEdge.setDestination((Vertex)busStops.get(nextBusRoute.getBusStopCode()));
            busEdge.setDistance(nextBusRoute.getDistance() - currentBusRoute.getDistance());
            
            if (busEdge.getSource() == null) {
                System.out.println(currentBusRoute.getBusStopCode());
                continue;
            } else if (busEdge.getDestination() == null) {
                System.out.println(nextBusRoute.getBusStopCode());
                continue;
            }
            
            busEdges.add(busEdge);
        }
        
        return busEdges;
    }
    
    public static List<MrtEdge> generateMrtEdges(List<MrtRoute> mrtRoutes, Map<String, MrtStation> mrtStations) {
        List<MrtEdge> mrtEdges = new ArrayList<>();
        
        for (int i = 0; i < mrtRoutes.size(); i++) {
            MrtRoute currentMrtRoute = mrtRoutes.get(i);
            
            MrtEdge mrtEdge = new MrtEdge();
            mrtEdge.setSource(mrtStations.get(currentMrtRoute.getSourceStation()));
            mrtEdge.setDestination(mrtStations.get(currentMrtRoute.getDestinationStation()));
            mrtEdge.setMrtLine(currentMrtRoute.getMrtLine());
            mrtEdge.setWeight(currentMrtRoute.getTime());
            mrtEdge.setDirection(currentMrtRoute.getDirection());
            mrtEdge.setTowards(currentMrtRoute.getTowards());
            
            if (mrtEdge.getSource() == null || mrtEdge.getDestination() == null) {
                continue;
            }
            
            mrtEdges.add(mrtEdge);
        }
        
        return mrtEdges;
    }
    
    public static void assignEdges(List<Vertex> vertexes, List<Edge> edges) {
        vertexes.stream().forEach((vertex) -> {
            List<Edge> vertexEdges = vertex.getEdges();
            for (int i = edges.size() - 1; i > -1; i--) {
                Edge edge = edges.get(i);
                if (edge.getSource().equals(vertex)) {
                    vertexEdges.add(edges.get(i));
                    edges.remove(i);
                }
            }
        });
    }
}
